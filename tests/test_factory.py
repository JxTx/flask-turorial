from flaskr import create_app


def test_config():
    '''
    If config is not passed, there should be some
    default configuration, otherwise the configuration
    should be overridden.
    '''
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing


def test_hello(client):
    '''
    You added the hello route as an example when writing
    the factory at the beginning of the tutorial. It returns
    “Hello, World!”, so the test checks that the response
    data matches.
    '''
    response = client.get('/hello')
    assert response.data == b'Hello, World!'
